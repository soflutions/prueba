package org.example;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import org.junit.Test;

/**
 * Unit test for simple App.
 */
public class PersonaTest
{
    /**
     * Rigorous Test :-)
     */
    @Test
    public void test()
    {
        Persona persona = new Persona("Douglas", "Gallardo");
        String value = persona.getNombreCompleto();

        assertEquals("Douglas Gallardo", value);
    }
}
