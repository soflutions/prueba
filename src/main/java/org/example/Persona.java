package org.example;

public class Persona {

    private String nombre;
    private String apellido;

    public Persona(){

    }

    public Persona(String nombre, String apellido) {
        this.nombre = nombre;
        this.apellido = apellido;
    }

    public String getNombreCompleto(){
        return this.nombre + " " + this.apellido;
    }


    public String getApellido() {
        return apellido;
    }

    public String getNombre() {
        return nombre;
    }
}
